--
-- Table structure for table `SDB_Category`
--

DROP TABLE IF EXISTS `SDB_Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SDB_Category` (
  `CategoryID` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(81) NOT NULL,
  PRIMARY KEY (`CategoryID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SDB_Code_Category_XREF`
--

DROP TABLE IF EXISTS `SDB_Code_Category_XREF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SDB_Code_Category_XREF` (
  `ID` mediumint(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(11) NOT NULL,
  `CategoryID` mediumint(11) DEFAULT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SDB_Courses`
--

DROP TABLE IF EXISTS `SDB_Courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SDB_Courses` (
  `CourseID` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `ClassNumber` mediumint(18) NOT NULL,
  `Title` varchar(92) NOT NULL,
  `CourseCode` varchar(8) NOT NULL,
  `Number` varchar(8) NOT NULL,
  `Section` varchar(8) NOT NULL,
  `GenEdType` varchar(8) NOT NULL,
  `StartDate` varchar(81) NOT NULL,
  `EndDate` varchar(81) NOT NULL,
  `Instructor` varchar(92) NOT NULL,
  `Location` varchar(32) NOT NULL,
  `Days` varchar(32) NOT NULL,
  `StartTime` varchar(32) NOT NULL,
  `EndTime` varchar(32) NOT NULL,
  `ForEducators` tinyint(1) NOT NULL DEFAULT '0',
  `ForHighSchool` tinyint(1) NOT NULL DEFAULT '0',
  `ForMaymester` tinyint(1) NOT NULL DEFAULT '0',
  `Notes` varchar(92) NOT NULL,
  `Term` varchar(11) NOT NULL,
  `Active` int(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date created or imported',
  PRIMARY KEY (`CourseID`),
  UNIQUE KEY `uq_SDB_Courses` (`ClassNumber`,`CourseCode`,`Number`,`Section`,`StartDate`,`EndDate`,`Location`,`Days`,`StartTime`,`EndTime`,`Instructor`)
) ENGINE=InnoDB AUTO_INCREMENT=10269 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SDB_Import`
--

DROP TABLE IF EXISTS `SDB_Import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SDB_Import` (
  `SOCNbr` int(11) NOT NULL,
  `Term` varchar(8) NOT NULL,
  `Subject` varchar(55) NOT NULL,
  `Num` varchar(55) NOT NULL,
  `Sec` varchar(55) NOT NULL,
  `Descr` varchar(55) NOT NULL,
  `ClassStat` varchar(16) NOT NULL,
  `StartTime` varchar(55) NOT NULL,
  `EndTime` varchar(55) NOT NULL,
  `Location` varchar(55) NOT NULL,
  `M` varchar(8) NOT NULL,
  `T` varchar(8) NOT NULL,
  `W` varchar(8) NOT NULL,
  `R` varchar(8) NOT NULL,
  `F` varchar(8) NOT NULL,
  `S` varchar(8) NOT NULL,
  `StartDate` varchar(55) NOT NULL,
  `EndDate` varchar(55) NOT NULL,
  `Instructor` varchar(55) NOT NULL,
  `GenEd` varchar(8) NOT NULL,
  `GenEd2` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SDB_Users`
--

DROP TABLE IF EXISTS `SDB_Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SDB_Users` (
  `UserID` mediumint(11) NOT NULL AUTO_INCREMENT,
  `FName` varchar(64) NOT NULL,
  `LName` varchar(64) NOT NULL,
  `AccessID` varchar(16) NOT NULL,
  `Email` varchar(64) NOT NULL,
  `UserRole` mediumint(11) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `AccessID` (`AccessID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

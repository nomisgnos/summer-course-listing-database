<?php
    // TODO- dynamic methods- one method for handling different queries and categories
    /**
     *SDB CourseMethods
     *
     *Contains all methods used by /summer/courses to execute DB queries and display events
     *
     */
    
    /**
     *SDB CourseMethods - Methods used for SDB registration
     *
     *Contains all methods used by /summer/courses to execute DB queries and display events
     *
     *@package SDB
     *@author Jay Vyas, jkv5@psu.edu
     *
     */
    Class CourseMethods {
        /**
         *__construct Constructor for Course Methods
         *
         *@param Core       $core       Core class object for DB
         *@param string[]   $settings   Array of settings
         *@param int        $userID     UserID of current user
         *@param int        $semesterID Current semesterID
         *@return void
         */
        public function __construct($core, $settings) {
            $this->core = $core;
            $this->settings = $settings;
        }

        public function getEducatorsCourses() {
            $query = "SELECT IF(crs.Days IS NULL or crs.Days = '', crs.Location, crs.Days) as displayedDays,
                            crs.*, ctg.* FROM SDB_Courses crs 
                        INNER JOIN SDB_Code_Category_XREF ON Code = CourseCode 
                        INNER JOIN SDB_Category ctg USING(CategoryID) 
                        WHERE ForEducators = 1 AND Active = 1
                        ORDER BY CourseCode";
            $results = $this->core->executeSQL($query);
            return $results;
        }

        public function getGenEdCourses() {
            $query = "SELECT IF(crs.Days IS NULL or crs.Days = '', crs.Location, crs.Days) as displayedDays,
                            crs.*, ctg.* FROM SDB_Courses crs 
                        INNER JOIN SDB_Code_Category_XREF ON Code = CourseCode 
                        INNER JOIN SDB_Category ctg USING(CategoryID) 
                        WHERE GenEdType != '' AND GenEdType IS NOT NULL AND Active = 1
                        ORDER BY GenEdType, CourseCode";
            $results = $this->core->executeSQL($query);
            return $results;
        }
        
        public function getMaymesterCourses() {
            $query = "SELECT IF(crs.Days IS NULL or crs.Days = '', crs.Location, crs.Days) as displayedDays,
                            crs.*, ctg.* FROM SDB_Courses crs 
                        INNER JOIN SDB_Code_Category_XREF ON Code = CourseCode 
                        INNER JOIN SDB_Category ctg USING(CategoryID) 
                        WHERE ForMaymester = 1 AND Active = 1
                        ORDER BY CourseCode";
            $results = $this->core->executeSQL($query);
            return $results;
        }
        
        public function getOnlineCourses() {
            $query = "SELECT IF(crs.Days IS NULL or crs.Days = '', crs.Location, crs.Days) as displayedDays,
                            crs.*, ctg.* FROM SDB_Courses crs 
                        INNER JOIN SDB_Code_Category_XREF ON Code = CourseCode 
                        INNER JOIN SDB_Category ctg USING(CategoryID) 
                        WHERE crs.ClassNumber IN (SELECT crs2.ClassNumber FROM SDB_Courses crs2 WHERE LOWER(crs2.Location) = 'web' AND crs2.Active = 1)
                        AND Active = 1
                        ORDER BY CourseCode";
            $results = $this->core->executeSQL($query);
            return $results;
        }
        
        public function getHighschoolCourses() {
            $query = "SELECT IF(crs.Days IS NULL or crs.Days = '', crs.Location, crs.Days) as displayedDays,
                            crs.*, ctg.* FROM SDB_Courses crs 
                        INNER JOIN SDB_Code_Category_XREF ON Code = CourseCode 
                        INNER JOIN SDB_Category ctg USING(CategoryID) 
                        WHERE ForHighSchool = 1 AND Active = 1
                        ORDER BY CourseCode";
            $results = $this->core->executeSQL($query);
            return $results;
        }
        
        public function getCoursesByCategory() {
            $query = "SELECT IF(crs.Days IS NULL or crs.Days = '', crs.Location, crs.Days) as displayedDays,
                            crs.*, ctg.* FROM SDB_Courses crs 
                        INNER JOIN SDB_Code_Category_XREF ON Code = CourseCode 
                        INNER JOIN SDB_Category ctg USING(CategoryID) 
                        WHERE Active = 1
                        ORDER BY Name, CourseCode";
            $results = $this->core->executeSQL($query);
            return $results;
        }
        
        public function getCategories() {
            $query = "SELECT ctg.Name FROM SDB_Courses crs 
                        INNER JOIN SDB_Code_Category_XREF ON Code = CourseCode 
                        INNER JOIN SDB_Category ctg USING(CategoryID) WHERE Active = 1 GROUP BY Name";
            $results = $this->core->executeSQL($query);
            return $results;
        }
        
        public function printCourseTable($courses, $summary, $caption) {
            $this->newCourseTableHead($summary, $caption);
            foreach($courses as $crs) {
                $this->newCourseTableBody($crs);
            }
            $this->newCourseTableFooter();
        }
        
        private function cleanString($str) {
            $str = str_replace(' ', '_', $str);
            return preg_replace('/[^A-Za-z0-9\-]/', '', $str);
        }
        
        private function listCategories() {
            $categories = $this->getCategories();
            
            echo "<ul>";
            foreach($categories as $category) {
                $href = $this->cleanString($category->Name);
                echo "<li><a href='#" . $href . "'>" . $category->Name . "</a></li>";
            }
            echo "</ul>";
        }
        
        public function printCourseTablesByCategory($courses, $caption) {
            $this->listCategories();
            
            $currID = -1;
            foreach($courses as $crs) {
                if($crs->CategoryID != $currID) {
                    if($currID != -1) {
                        $this->newCourseTableFooter();
                    }
                    
                    $href = $this->cleanString($crs->Name);
                    echo "<h2 id=" . $href .">" . $crs->Name . "</h2>";
                    $this->newCourseTableHead($crs->Name, $caption);
                    $currID = $crs->CategoryID;
                }
                
                $this->newCourseTableBody($crs);
            }
        }
        
        private function newCourseTableHead($name, $caption) {
            ?>
            <div class="table-responsive">
                <table style="display:none;" class="table table-hover dataCourseTable" summary="<?=$name?> courses">
                    <caption><?=$caption?></caption>
                    <thead>
                        <tr>
                            <th scope="col">Class Nbr</th>
                            <th scope="col">Course</th>
                            <th scope="col">Title</th>
                            <!--<th scope="col">Days</th>-->
                            <th scope="col">Gen Ed</th>
                            <th scope="col">Start Date</th>
                            <th scope="col">End Date</th>
                        </tr>
                    </thead>
                    <tbody>
            
            <?php
        }
        
        private function newCourseTableBody($crs) {
            ?>
            <tr class="odd">
                <td align="right"><?=$crs->ClassNumber?></td>
                <td><?=$crs->CourseCode?> <?=$crs->Number?>.<?=$crs->Section?></td>
                <td><?=$crs->Title?></td>
                <!--<td><?=$crs->displayedDays?></td>-->
                <td><?=$crs->GenEdType?></td>
                <td align="right"><?=$crs->StartDate?></td>
                <td align="right"><?=$crs->EndDate?></td>
            </tr>
            <?php
        }
        
        private function newCourseTableFooter() {
            ?>
                    </tbody>
                </table>
            </div>
            <?php
        }
        
    }

?>
<?php
    /**
     *SDB Admin Methods
     *
     *Contains all the admin methods used by /summer/admin
     *to execute DB query, print navigation, and other
     *utility functions
     *
     *@author Jay Vyas, jkv5@psu.edu
     *@package SDB
     */
     
    /**
     *SDB Admin Methods
     *
     *Contains all the admin methods used by /summer/admin
     *to execute DB query, print navigation, and other
     *utility functions
     *
     */
    Class AdminMethods {
        /**
         *  __construct AdminMethods constructor
         *  
         *@param Core $core     Database class
         *@param string[] $settings Settings array
         *@param integer $userID   UserID of current user
         *@return void
         *  
         */
        public function __construct($core, $settings, $userID) {
            $this->core = $core;
            $this->settings = $settings;
            $this->userID = $userID;
        }

        /* TODO- make nav menu dynamic through these methods
        private function newNavItem($title, $icon, $url) {
        ?>
            <div class="btn-group btn-group-nav">
                <a id="nav-<?=$title?>" href="<?=$this->settings['current_URL_path']?><?=$url?>" class="btn btn-nav">
                    <span class='<?=$icon?>'></span>
                    <h4><?=$title?></h4>
                </a>
            </div>
        <?php
        }
        

        public function printNewNavigation($roleID) {
            $this->newNavItem("Dashboard", "fa fa-dashboard", "/admin");
            $this->newNavItem("Users", "fa fa-users", "/admin/users");
            $this->newNavItem("Courses", "fa fa-list", "/admin/course");
            $this->newNavItem("Mappings", "fa fa-map", "/admin/map");
            $this->newNavItem("Reports", "fa fa-file-text-o", "/admin/reports");
            $this->newNavItem("Import", "fa fa-cloud-upload", "/admin/import");
        }
        */
        
        /**
         *  getUsers Fetches all users who are not student
         *  
         *@return null|object[] DB objects of all users (*)
         */
        public function getUsers() {
            $query = 'SELECT * FROM SDB_Users';

            $users = $this->core->executeSQL($query);
            return (count($users) > 0) ? $users : null;
        }

        /**
         *  addUser Adds user to users table, adds user roles to user roles table
         *  
         *@param string[] $post         Post request array of values to use for insert
         *@return int Response of DB query
         */
        public function addUser($post, $imported = false) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':fname';
            $arr[$i]['value'] = $post['firstname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':lname';
            $arr[$i]['value'] = $post['lastname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':accessid';
            $arr[$i]['value'] = $post['accessid'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':email';
            $arr[$i]['value'] = $post['email'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            
            $query = 'INSERT INTO SDB_Users
                  (FName, LName, AccessID, Email, UserRole, createdOn)
                  VALUES (:fname, :lname, :accessid, :email, 1, now())';

            $response = $this->core->executeSQL($query, $arr, 'all', 1, true, true);
            return $response;
        }
        
        /**
         *  editUser Edits user based on submitted values
         *  
         *@param string[] $post Post request array of submitted values
         *@return int Response of DB query
         */
        public function editUser($post) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':fname';
            $arr[$i]['value'] = $post['firstname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':lname';
            $arr[$i]['value'] = $post['lastname'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $post['userid'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':email';
            $arr[$i]['value'] = $post['email'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            
            $query = 'UPDATE SDB_Users
                      SET FName=:fname, LName=:lname, Email=:email WHERE UserID=:userid';

            $response = $this->core->executeSQL($query, $arr);            
            return $response;
        }
        
        /**
         *  deleteUser Removes user from DB including their user roles
         *  
         *@param int $userID UserID of user
         *@return null|int DB response of query
         */
        public function deleteUser($userID) {
            if(empty($userID)) {
                return null;
            }
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':userid';
            $arr[$i]['value'] = $userID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM SDB_Users
                      WHERE UserID = :userid';
                      
            return $this->core->executeSQL($query, $arr);
        }
        
        public function deleteCourseCode($id) {
            if(empty($id)) {
                return null;
            }
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':codeid';
            $arr[$i]['value'] = $id;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM SDB_Code_Category_XREF
                      WHERE ID = :codeid';
                      
            return $this->core->executeSQL($query, $arr);
        }
        
        public function deleteCourse($id) {
            if(empty($id)) {
                return null;
            }
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':crsid';
            $arr[$i]['value'] = $id;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM SDB_Courses
                      WHERE CourseID = :crsid';
                      
            return $this->core->executeSQL($query, $arr);
        }
        
        public function deleteCategory($id) {
            if(empty($id)) {
                return null;
            }
            
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':ctgid';
            $arr[$i]['value'] = $id;
            $arr[$i]['data_type'] = PDO::PARAM_INT;

            $query = 'DELETE FROM SDB_Category
                      WHERE CategoryID = :ctgid';
                      
            return $this->core->executeSQL($query, $arr);
        }
        
        public function setSelectedCategory($categories, $selectedCategory) {
            $search = "<option value='" . $selectedCategory . "'>";
            $replace = "<option value='" . $selectedCategory . "' selected>";

            return str_replace($search, $replace, $categories);
        }
        
        private function buildCategories($categories) {
            $html = '';
            foreach($categories as $category) {
                $html .= "<option value='" . $category->CategoryID ."'>" . $category->Name . "</option>";
            }
            return $html;    
        }
        
        public function updateCodeMappings($updateValues) {
            $i = 0; $arr = null;
            
            $query = 'UPDATE SDB_Code_Category_XREF SET CategoryID = CASE ID';
            $codes = "(";
            
            foreach($updateValues as $key => $categoryID) {
                $codeID = str_replace("CourseCategory", "",  $key);
                $codes .= ':code' . $codeID . ",";
                
                $arr[$i]['parameter'] = ':code' . $codeID;
                $arr[$i]['value'] = $codeID;
                $arr[$i]['data_type'] = PDO::PARAM_INT;
                $i++;
                $arr[$i]['parameter'] = ':category' . $categoryID;
                $arr[$i]['value'] = $categoryID;
                $arr[$i]['data_type'] = PDO::PARAM_INT;
                $i++;
                
                $query .= " WHEN :code" . $codeID . " THEN :category" . $categoryID;
            }
            $codes = trim($codes, ",");
            $query .= " ELSE CategoryID END WHERE ID IN " . $codes . ")"; 

            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }
        
        public function updateCourseTypes($courseID, $types) {

            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':crsid';
            $arr[$i]['value'] = $courseID;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':fored';
            $arr[$i]['value'] = $types->CourseEducator;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':forhs';
            $arr[$i]['value'] = $types->CourseHighSchool;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':formay';
            $arr[$i]['value'] = $types->CourseMaymester;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            

            $query = "INSERT INTO SDB_CoursesOf 
                      (`ClassNumber`, `Location`, `ForEducators`, `ForHighSchool`, `ForMaymester`, `updatedOn`)
                      SELECT 
                        c.`ClassNumber`, c.`Location`, :fored, :forhs, :formay, now()
                      FROM 
                        SDB_Courses c
                      WHERE 
                        c.CourseID= :crsid
                      ON DUPLICATE KEY UPDATE
                        `ForEducators` = VALUES(ForEducators),
                        `ForHighSchool` = VALUES(ForHighSchool),
                        `ForMaymester` = VALUES(ForMaymester),
                        `updatedOn` = now()";

            $this->core->executeSQL($query, $arr);

            $query = "UPDATE SDB_Courses 
                        SET ForEducators=:fored, ForHighSchool=:forhs, ForMaymester=:formay
                        WHERE CourseID=:crsid";

            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }
        
        public function handleAddCourseCategory($categoryName) {
            if(empty($categoryName)) {
                $msg = "Error: Invalid category name.";
                $this->jsAlert("error", $msg);
            } else {
                $this->addCourseCategory($categoryName);
                $msg = "Success! Category: " . $categoryName . " has been added.";
                $this->jsAlert("success", $msg);
            }
        }

        public function handleAddCourseCode($courseCode) {
            if(empty($courseCode)) {
                $msg = "Error: Invalid course code.";
                $this->jsAlert("error", $msg);
            } else {
                $this->addCourseCode($courseCode);
                $msg = "Success! Course code: " . $courseCode . " has been added.";
                $this->jsAlert("success", $msg);
            }
        }

        public function handleUpdateCodeMappings($updateValues) {
            $this->updateCodeMappings($updateValues);
            $msg = "Success! Course Mappings updated.";
            $this->jsAlert("success", $msg);
        }
        
        public function handleUpdateCategories($updateValues) {
            $this->updateCategories($updateValues);
            $msg = "Success! Course categories updated.";
            $this->jsAlert("success", $msg);
        }
        
        public function updateCategories($updateValues) {
            $i = 0; $arr = null;
            
            $query = 'UPDATE SDB_Category SET Name = CASE CategoryID';
            $ids = "(";
            
            foreach($updateValues as $key => $categoryName) {
                $id = str_replace("CourseCategory", "",  $key);
                $ids .= ':ctg' . $id . ",";
                
                $arr[$i]['parameter'] = ':ctg' . $id;
                $arr[$i]['value'] = $id;
                $arr[$i]['data_type'] = PDO::PARAM_INT;
                $i++;
                $arr[$i]['parameter'] = ':ctgName' . $id;
                $arr[$i]['value'] = $categoryName;
                $arr[$i]['data_type'] = PDO::PARAM_STR;
                $i++;
                
                $query .= " WHEN :ctg" . $id . " THEN :ctgName" . $id;
            }
            $ids = trim($ids, ",");
            $query .= " ELSE Name END WHERE CategoryID IN " . $ids . ")"; 

            return $this->core->executeSQL($query, $arr, 'all', 1, true, true);
        }
        
        public function getCourseCategories($build = false) {
            $query = 'SELECT * 
                      FROM SDB_Category
                      ORDER by Name';
                      
            $categories = $this->core->executeSQL($query);
            
            if($build) {
                return $this->buildCategories($categories);
            }
            
            return (count($categories) > 0) ? $categories : NULL;
        }
        
        public function buildCategoryNames($categories) {
            $arr = [];
            foreach($categories as $category) {
                $index = 'CourseCategory' . $category->CategoryID;
                $arr[$index] = ($category->CategoryID == NULL) ? "" : $category->Name;
            }
            return $arr;
        }
        
        public function buildCodeToCategoryOptions($codes) {
            $arr = [];
            foreach($codes as $code) {
                $index = 'CourseCategory' . $code->ID;
                $arr[$index] = ($code->CategoryID == NULL) ? "" : $code->CategoryID;
            }
            return $arr;
        }
        
        public function getCourseCodes() {
            $query = 'SELECT * 
                      FROM SDB_Code_Category_XREF
                      LEFT JOIN SDB_Category USING(CategoryID)
                      ORDER by Code';
                      
            $codes = $this->core->executeSQL($query);
            
            return (count($codes) > 0) ? $codes : NULL;
        }
        
        public function getCourseInfo($courseid) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':crsid';
            $arr[$i]['value'] = $courseid;
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $query = 'SELECT cors.*
                      FROM SDB_Courses cors
                      LEFT JOIN SDB_Code_Category_XREF cxref ON cxref.Code = cors.CourseCode
                      WHERE CourseID=:crsid';
                      
            $codes = $this->core->executeSQL($query, $arr);
            
            return (count($codes) > 0) ? $codes : NULL;
        }
        
        public function getCourses() {
            $query = 'SELECT cors.*
                      FROM SDB_Courses cors
                      LEFT JOIN SDB_Code_Category_XREF cxref ON cxref.Code = cors.CourseCode';
                      
            $codes = $this->core->executeSQL($query);
            
            return (count($codes) > 0) ? $codes : NULL;
        }
        
        public function addCourse($post) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':ClassNumber';
            $arr[$i]['value'] = $post['ClassNumber'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':Title';
            $arr[$i]['value'] = $post['Title'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':CourseCode';
            $arr[$i]['value'] = $post['CourseCode'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':CourseNumber';
            $arr[$i]['value'] = $post['CourseNumber'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':Section';
            $arr[$i]['value'] = $post['Section'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':GenEd';
            $arr[$i]['value'] = $post['GenEd'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Instructor';
            $arr[$i]['value'] = $post['Instructor'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Location';
            $arr[$i]['value'] = $post['Location'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Days';
            $arr[$i]['value'] = $post['Days'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':StartDate';
            $arr[$i]['value'] = $post['StartDate'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':EndDate';
            $arr[$i]['value'] = $post['EndDate'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':StartTime';
            $arr[$i]['value'] = $post['StartTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':EndTime';
            $arr[$i]['value'] = $post['EndTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Active';
            $arr[$i]['value'] = $post['Active'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Notes';
            $arr[$i]['value'] = $post['Notes'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            $query = 'INSERT INTO SDB_Courses 
                (ClassNumber, Title, CourseCode, Number, Section, GenEdType, StartDate, 
                EndDate, Instructor, Location, Days, StartTime, EndTime, Notes, Active)
                VALUES (:ClassNumber, :Title, :CourseCode, :CourseNumber, :Section, :GenEd, 
                :StartDate, :EndDate, :Instructor, :Location, :Days, :StartTime, :EndTime, :Notes, :Active)';
            
            return $this->core->executeSQL($query, $arr, 'all', 1, true, true);
        }
        
        public function editCourse($post) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':CourseID';
            $arr[$i]['value'] = $post['CourseID'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':ClassNumber';
            $arr[$i]['value'] = $post['ClassNumber'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':Title';
            $arr[$i]['value'] = $post['Title'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':CourseCode';
            $arr[$i]['value'] = $post['CourseCode'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':CourseNumber';
            $arr[$i]['value'] = $post['CourseNumber'];
            $arr[$i]['data_type'] = PDO::PARAM_INT;
            $i++;
            $arr[$i]['parameter'] = ':Section';
            $arr[$i]['value'] = $post['Section'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':GenEd';
            $arr[$i]['value'] = $post['GenEd'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Instructor';
            $arr[$i]['value'] = $post['Instructor'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Location';
            $arr[$i]['value'] = $post['Location'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Days';
            $arr[$i]['value'] = $post['Days'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':StartDate';
            $arr[$i]['value'] = $post['StartDate'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':EndDate';
            $arr[$i]['value'] = $post['EndDate'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':StartTime';
            $arr[$i]['value'] = $post['StartTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':EndTime';
            $arr[$i]['value'] = $post['EndTime'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Active';
            $arr[$i]['value'] = $post['Active'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            $i++;
            $arr[$i]['parameter'] = ':Notes';
            $arr[$i]['value'] = $post['Notes'];
            $arr[$i]['data_type'] = PDO::PARAM_STR;

            $query = 'UPDATE SDB_Courses
                      SET ClassNumber=:ClassNumber, Title=:Title, CourseCode=:CourseCode, Number=:CourseNumber, Section=:Section, Location=:Location, Days=:Days, StartTime=:StartTime, EndTime=:EndTime, 
                      StartDate=:StartDate, EndDate=:EndDate, GenEdType=:GenEd, Instructor=:Instructor, Notes=:Notes, Active=:Active
                      WHERE CourseID = :CourseID';
            
            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }
        
        public function addCourseCode($courseCode) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':code';
            $arr[$i]['value'] = $courseCode;
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            
            $query = 'INSERT INTO SDB_Code_Category_XREF
                      (Code)
                      VALUES (:code)';

            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }
        
        public function addCourseCategory($categoryName) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':category';
            $arr[$i]['value'] = $categoryName;
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            
            $query = 'INSERT INTO SDB_Category
                      (Name)
                      VALUES (:category)';

            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }

        public function purgeCourses() {
            $query = 'DELETE FROM SDB_Courses';
            return $this->core->executeSQL($query);
        }

        public function purgeCoursesCategories() {
            $query = 'UPDATE SDB_Courses SET ForEducators = 0, ForHighSchool = 0, ForMaymester = 0 WHERE ForEducators = 1 OR ForHighSchool = 1 OR ForMaymester = 1';
            $this->core->executeSQL($query);
            $query = 'DELETE FROM SDB_CoursesOf';
            $this->core->executeSQL($query);
            return true;
        }

        public function purgeCategories() {
            $query = 'DELETE FROM SDB_Category';
            return $this->core->executeSQL($query);
        }
        
        public function purgeCodes() {
            $query = 'DELETE FROM SDB_Code_Category_XREF';
            return $this->core->executeSQL($query);
        }
        
        public function purgeCodeCategories() {
            $query = 'UPDATE SDB_Code_Category_XREF SET CategoryID=NULL';
            return $this->core->executeSQL($query);
        }
        
        public function purgeAll() {
            $this->backupDB('purge');
            $this->purgeCourses();
            $this->purgeCategories();
            $this->purgeCodes();
            $this->purgeCoursesCategories();
        }
        
        public function backupDB($fileSuffix='',$download=0) {
            $path = dirname(__FILE__) . "/admin/import/exports/";

            // defaults from settings
            $host     = $this->core->settings['dbHost'];
            $user     = $this->core->settings['dbUsername'];
            $password = $this->core->settings['dbPassword'];
            $database = $this->core->settings['dbName'];
            
            $dateString = date("Ymd_Gis");
            $path .= $dateString . "_" . $database . "_" . $fileSuffix .".sql";
            
            $file = exec('mysqldump --host=\''. $host .'\' --user=\''. $user .'\' --password=\''. $password .'\' '. $database .' --add-drop-table --single-transaction > '. $path);
            if($download && file_exists($path)) {
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=" . basename($path) . "");
                header("Content-Length: " . filesize($path));
                header("Content-Type: application/octet-stream;");
                readfile($path);
                exit;
            }
            
            return $file === 0;
        }
        
        public function getName($accessID) {
            $i = 0; $arr = null;
            $arr[$i]['parameter'] = ':access';
            $arr[$i]['value'] = $accessID;
            $arr[$i]['data_type'] = PDO::PARAM_STR;
            
            $query = 'SELECT CONCAT(FName, " ", LName) as FullName
                        FROM SDB_Users
                        WHERE AccessID=:access';
                        
            return $this->core->executeSQL($query, $arr, 'all', 1, false, true);
        }
        
        public function jsAlert($type, $msg, $title="") {
            echo "<script>newAlert('" . $type . "', '" . $msg . "', '" . $title . "');</script>";
        }
        
        /**
         *  base64_url_encode Encodes input string with base64_encode- used for passing IDs and other information quickly without executing DB query
         *  
         *@param string $input String to encode
         *@return string Encoded string
         */
        public function base64_url_encode($input) {
            return strtr(base64_encode($input), '+/=', '-_,');
        }
        
        /**
         *  base64_url_decode Decodes encoded input string with base64_decode- used for passing IDs and other information quickly without executing DB query
         *  
         *@param string $input String to encode
         *@return string Decoded string
         */
        public function base64_url_decode($input) {
            return base64_decode(strtr($input, '-_,', '+/='));
        }
    }

?>

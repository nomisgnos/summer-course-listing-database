<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<meta name="robots" content="noindex">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="HandheldFriendly" content="true" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>%TITLE%</title>
        <link href="<?= $_settings['current_URL_path'];?>/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        
        <style>html{overflow-y: scroll;}</style>
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100%7COpen+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800&amp;subset=latin,latin" media="all"/>


      <!-- New version of jQuery -->
        <script src="<?=$_settings['current_URL_path'];?>/resources/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/js/jquery.scrollUp.min.js"></script>
        <link href="<?=$_settings['current_URL_path'];?>/resources/css/toastr.min.css" rel="stylesheet"/>
    <script src="<?=$_settings['current_URL_path'];?>/resources/js/toastr.min.js"></script>
    <script>
        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": true,
          "progressBar": false,
          "positionClass": "toast-top-center",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
    </script>
      <!-- jQuery scrollUp -->
        <script>
            $(function () {
              $.scrollUp({
                scrollName: 'scrollUp',
                topDistance: '300',
                topSpeed: 300,
                animation: 'fade',
                animationInSpeed: 200,
                animationOutSpeed: 200,
                scrollText: '',
                activeOverlay: false, 
              });
            });
        </script>
        
        <script src="https://harrisburg2.vmhost.psu.edu/scripts/iframeResizer.contentWindow.min.js"></script>
        
      <!-- Bootstrap -->
        <link id="scrollUpTheme" href="<?= $_settings['current_URL_path'];?>/resources/css/jquery.scrollUp.css" rel="stylesheet">

        <link href="<?= $_settings['current_URL_path'];?>/resources/bootstrap/css/bootstrap-select.min.css" rel="stylesheet">
        <script src="<?= $_settings['current_URL_path'];?>/resources/bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
        

        <link rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/css/font-awesome.min.css" type="text/css" />

        <link rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/css/inherited_styles.css" type="text/css" />
        <?php
            // Resources for Admin section
            if (strpos(strtolower($circuit), 'admin') !== false) {
                echo '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/custom.min.css" type="text/css" />' . PHP_EOL;

                echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/custom.min.js" charset="utf-8"></script>' . PHP_EOL;
                
                echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/pace.min.js" charset="utf-8"></script>' . PHP_EOL;
                echo '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/pace-theme-minimal.css" type="text/css" />' . PHP_EOL;

                echo '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/admin.css" type="text/css" />' . PHP_EOL;

                    /* Bootstrap-select */
                echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/bootstrap/js/bootstrap-select.min.js" charset="utf-8"></script>' . PHP_EOL;
                
                    /* Handle Forms with AJAX */
                echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/Forms.min.js" charset="utf-8"></script>' . PHP_EOL;
                
                    /* Handle Admin Forms with AJAX */
                echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/AdminForms.min.js" charset="utf-8"></script>' . PHP_EOL;

                    /* Bootstrap Datepicker */
                echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/bootstrap-datepicker.min.js" charset="utf-8"></script>' . PHP_EOL;
                echo "\t" . '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/bootstrap-datepicker3.min.css" type="text/css" />' . PHP_EOL;
                
                    /* Bootstrap Clockpicker */
                echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/bootstrap-clockpicker.min.js" charset="utf-8"></script>' . PHP_EOL;
                echo "\t" . '<link rel="stylesheet" href="' . $_settings['current_URL_path'] . '/resources/css/bootstrap-clockpicker.min.css" type="text/css" />' . PHP_EOL;
            }
            
                /* Bootbox- Simplified handling for confirmation modals */
            echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/bootstrap/js/bootbox.min.js" charset="utf-8"></script>' . PHP_EOL;
            /* JS Utils */
            echo "\t" . '<script src="' . $_settings['current_URL_path'] . '/resources/js/Utils.min.js" charset="utf-8"></script>' . PHP_EOL;
        ?>

        <!-- Custom Stylesheets -->
        <link rel="stylesheet" href="<?= $_settings['current_URL_path'];?>/resources/css/styles.css" type="text/css" />
        <link rel="stylesheet" href="<?=$_settings['current_URL_path'];?>/resources/css/bootstrap-style.css" type="text/css" />
        
        <!-- dataTables -->
        <link rel="stylesheet" type="text/css" href="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.css"/>
    </head>
 

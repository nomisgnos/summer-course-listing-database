                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<footer>
<?php return; ?>
    <div id="footer-third-outer-wrapper" class="outer-wrapper clearfix">
          <div id="footer-layout-copy" class="footer-layout inner-wrapper clearfix">
            <div id="footer-third" class="region--footer-third">
              
              <div id="copyright-bar" class="clearfix">
                              <div class="footer-logo"><map name="footerlogo"><area shape="rect" coords="3,5,41,51" href="http://www.psu.edu"><area shape="rect" coords="42,0,116,51" href="https://harrisburg.psu.edu/"></map><img usemap="#footerlogo" src="https://harrisburg.psu.edu/profiles/psucampus/themes/psu_campus_base/images/footer_logo_new.png" alt="" title=""><a href="#skip-link" id="sitewide-back-to-top">Top</a></div>
                                          <div class="mobile-footer-logo"><map name="mobilefooterlogo"><area shape="rect" coords="4,9,53,61" href="http://www.psu.edu"><area shape="rect" coords="54,0,150,61" href="https://harrisburg.psu.edu/"></map><img usemap="#mobilefooterlogo" src="https://harrisburg.psu.edu/profiles/psucampus/themes/psu_campus_base/images/footer_logo_new.png" alt="" title=""></div>
                            <div class="copy-left clearfix">

                                  <nav class="nav-footer-info nav-footer">
                        <ul id="footer-info-menu" class="links clearfix inline"><li class="menu-341 first"><a href="https://psu.jobs/harrisburg/jobs">Employment</a></li>
    <li class="menu-1280"><a href="https://harrisburg.psu.edu/campus-map">Map</a></li>
    <li class="menu-1269"><a href="http://www.psu.edu/hotlines">Hotlines</a></li>
    <li class="menu-1279"><a href="https://harrisburg2.vmhost.psu.edu/calendar">Calendar</a></li>
    <li class="menu-1281"><a href="https://harrisburg.psu.edu/campus-directory">Directory</a></li>
    <li class="menu-338 last"><a href="https://harrisburg.psu.edu/search/gss">Search</a></li>
    </ul>                </nav><!--end .nav-->
                                <div class="address-copy">
                    <address class="vcard">
                       <div class="adr">
                          <span class="street-address">777 West Harrisburg Pike, </span><span class="locality">Middletown</span>, <span class="region">PA</span> <span class="postal-code">17057</span> | <span class="tel">717-948-6000</span>
                       </div>
                    </address>
                  </div>
                </div>
                <div class="copy-right clearfix">
                                  <nav class="nav-footer-legal nav-footer">
                        <ul id="footer-legal-menu" class="links clearfix inline"><li class="menu-327 first"><a href="http://www.psu.edu/web-privacy-statement">Privacy</a></li>
    <li class="menu-317"><a href="http://guru.psu.edu/policies/AD85.html">Non-discrimination</a></li>
    <li class="menu-316"><a href="http://guru.psu.edu/policies/OHR/hr11.html">Equal Opportunity</a></li>
    <li class="menu-329"><a href="https://harrisburg.psu.edu/accessibility-statement">Accessibility</a></li>
    <li class="menu-326 last"><a href="http://www.psu.edu/copyright-information">Copyright</a></li>
    </ul>                </nav><!--end .nav-->
                  
                  <div class="copyright-copy">The Pennsylvania State University © 2017</div>
                </div>

              </div>
            </div>
          </div> <!-- /#footer -->
        </div>
</footer>
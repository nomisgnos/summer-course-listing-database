<?php
/**
 *Methods for SDB Import tool
 *
 *Contains all methods used by Import tool to execute DB queries
 *
 */
 
/**
 *Methods for SDB Import tool
 *
 *Contains all methods used by Import tool to execute DB queries
 *
 *@package SDB
 *@author Jay Vyas, jkv5@psu.edu
 */
Class ImportMethods {
    
    /**
     *@var string $absoluteFileName File name for export data to csv
     */
    private $absoluteFileName = '';
    
    /**
     *@var string $baseDir Directory to export CSV files
     */
    private $baseDir = '/summer/includes/admin/import/exports/';
    
    /**
     *@var object $adm AdminMethods class object
     */
    private $adm;
    
    private $excel;
    private $data;
    private $finalData;
    private $headers;
    
    private $fileName;
    private $headerNumber;
    public  $fileLocation; 
    
    /**
     *@var string[] $keys Expected header columns in array format- MUST match import table column ORDER!! (mismatch name is ok)
     */
    private $keys = array(1 => "SOC Nbr", "Term", "Subject", "Num", "Sec", "Descr", "Class Stat", "Begin Time", "End Time", "Location", "M", "T", "W", "R", "F", "S", "Start Date", "End Date", "Instructor", "Gen Ed");

    /**
     *@var string BASE_FILE_NAME Primary file name for CSV file export, appended at end
     */
    const BASE_FILE_NAME = 'sdb_import.csv';
    
    /**
     *__construct Constructor for ImportMethods
     *
     *@param AdminMethods $adm Class object of AdminMethods
     *@return void
     */
    public function __construct($adm) {
        $this->adm = $adm;
        $this->fileLocation = __DIR__ . "/admin/import/uploads/";
    }
    
    /**
     *excelParseFile Reads excel file data and parses data by combining keys with rows/columns data
     *
     *@param string $name           File name of excel file to read
     *@return string[][] Parsed data 
     */
    public function excelParseFile($fileName, $headerNumber=1) {
        if(empty($fileName)) {
            return false;
        }
        
        $this->excel = new Spreadsheet_Excel_Reader($this->fileLocation . $fileName);
        
        $this->headerNumber = $headerNumber;
        $this->fileName = $fileName;
        
        $sheet = $this->excel->sheets[0];
        $cells = $sheet['cells'];
        $this->headers = $this->excel->getRow($this->headerNumber);
        
        // Fetch all rows starting at headerNumber
        $this->data = $this->excel->getData($this->headerNumber);
        
        if(empty($this->data)) {
            return false;
        }
        
        // Validate header based on expected keys and columns found
        if(!$this->verifyDataHeader($this->headers)) {
            $this->remapKeys($this->headers, $this->data[$this->headerNumber+1]);
            return false;
        }

        $this->finalData = $this->combineHeaderValues();
    }
    
    public function combineHeaderValues() {
        // Combine header array as KEYS and row array as VALUES
        for($x = $this->headerNumber+1; $x <= $this->excel->rowcount(); $x++) {
            $a = array_combine($this->keys, $this->data[$x]);
            $newCells[] = $a;
        }
        
        return $newCells;
    }
    
    public function reformatData($order) {
        if($this->excel == NULL && !empty($order['FileName'])) {
            $this->excel = new Spreadsheet_Excel_Reader($this->fileLocation . $order['FileName']);
            $this->headerNumber = $order['HeaderNumber'];
        }
        
        unset($order['FileName']);
        unset($order['HeaderNumber']);
        
        $orderData = $this->excel->getRowsFromCols($order, $this->headerNumber+1);
        if(!empty($orderData)) {
            $this->data = $orderData;
        }

        $this->finalData = $this->combineHeaderValues();
    }
    
    private function setHeaderKey($foundList, $selectedKey) {
        $search = "data-key-name='" . $selectedKey . "'>";
        $replace = "data-key-name='" . $selectedKey . "' selected>";

        return str_replace($search, $replace, $foundList);
    }
    
    private function buildHeaderKeys($header) {
        $foundList = '';
        
        foreach($header as $i => $headerKey) {
            $foundList .= "<option value='" . $i ."' data-key-name='" . $headerKey . "'>" . $headerKey . "</option>";
        }
        return $foundList;    
    }
    
    private function renameDuplicateKeys($header) {
        $clear = [];
        foreach($header as $i => $key) {
            if(in_array($key, $clear)) {
                $key .= " (column " . $i . ")";
            }
            $clear[$i] = $key;
        }
        return $clear;
    }
    
    private function remapKeys($header, $firstData) {
        ?>
        <h3>Remap columns to expected keys<br><small>Please match expected header keys with columns found. A sample data is provided from row 1 of imported file for matching purposes.</small></h3>
        
        <form action="" method="post">
            <table id="headerMap" class="table table-hover">
                <thead>
                    <tr><th>Header keys</th><th>Columns found</th><th>Row 1 - sample data found</th></tr>
                </thead>
                <tbody>
                <?php
                    // rename duplicate keys before combining header keys and sample data
                    $clearHeader = $this->renameDuplicateKeys($header);
                    
                    // build list of options for header columns found
                    $headerList = $this->buildHeaderKeys($clearHeader);
                    
                    // combine header keys with sample data
                    $combined = array_combine($clearHeader, $firstData);
                    
                    foreach($this->keys as $i => $key) {
                        ?>
                        <tr>
                            <td><?=$key?></td>
                            <td>
                                <select required id="HeaderKey<?=$i?>" name="HeaderKey<?=$i?>" onChange="updateExampleData(this);" title="Select column" class="selectpicker form-control">
                                    <?=$this->setHeaderKey($headerList, $key)?>
                                </select>
                            </td>
                            <td id="HeaderData" name="HeaderData">
                                <?=$combined[$key]?>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
                    
                </tbody>
            </table>
            <input type="hidden" name="FileName" value="<?=$this->fileName?>"/>
            <input type="hidden" name="HeaderNumber" value="<?=$this->headerNumber?>"/>
            <div class="col-md-offset-9 pull-right"><button class="btn btn-info" type="submit" name="SubmitHeaderMapForm" value="Apply" >Apply changes</button>&nbsp;&nbsp;&nbsp;<button name="reset" class="btn btn-danger" type="reset" value="Reset" >Reset</button></div>
        </form>
        <?php // Updates sample data onChange?>
        <script>
            $('form').on('reset', function() {
              setTimeout(function() {
                $('.selectpicker').selectpicker('refresh');
              });
            });
            
            var dangerColor = '#f2dede';
            $('.selectpicker').each(function() {
                if($(this).selectpicker('val') == "") {
                    $(this).closest('tr').children('td,th').css('background-color', dangerColor);
                    $(this).data('empty-alert', true);
                }
            });
            
            var arr = <?=json_encode($combined)?>;
            function updateExampleData(sel) {
                if($(sel).data('empty-alert')) {
                    $(sel).closest('tr').children('td,th').css('background-color','#fff');
                }
                
                sid = $(sel).find('option:selected').data('key-name');
                $(sel).parent().parent().next('#HeaderData').html(arr[sid]);
            }
        </script>
        <?php
    }
    
    /**
     *verifyDataHeader Verify keys/columns of the row to make sure they are defined 
     *
     *@param string[] $header   Array of header keys found in file
     *@return bool
     */
    private function verifyDataHeader($header) {
        // Probably wrong header row number
        if(empty($header))
            return false;
        
        return !array_diff_key(array_flip($this->keys), array_flip($header));
    }
    
    /**
     *pushData Writes data to CSV and imports that into DB
     *
     *@param void $data Import data to insert into DB
     *@return void
     */
    public function pushData($data) {
        // Export data to CSV file
        $this->writeToCSV($data);
        
        // Import data to import table with generated CSV file
        $rowsImported = $this->loadDataInfile($this->absoluteFileName);
        
        $this->displayNumRows("Imported Rows: " . $rowsImported);
    }
    
    public function getAllMissingMaps() {
        $query = "SELECT * FROM SDB_Code_Category_XREF WHERE CategoryID IS NULL";
        $response = $this->adm->core->executeSQL($query);
        return $response;
    }
    
    public function verifyImport($missingMaps) {
        $allMissingMaps = $this->getAllMissingMaps();
        
        if($missingMaps > 0) {
            echo "<h3>Added " . $missingMaps . " course codes.</h3>";
        }
        
        if(count($allMissingMaps) > 0) {
            echo "<div class='panel panel-danger'>
                <div class='panel-heading'>
                    <h3 class='panel-title'>Following course codes are missing category connection</h3></div>
            
                    <ul class='list-group'>";
                        
                    foreach($allMissingMaps as $map) {
                        echo "<li class='list-group-item'>" . $map->Code . "<span style='float:right'>created on " . $map->createdOn . "</span></li>";
                    }
                    
                    echo "</ul>
                    <div class='panel-body'>
                        <h4>Please link course categories to course codes: <u><a href='map'>Code <i class='fa fa-arrow-right'></i> Category</a></u></h4>
                    </div>
            </div>";
            
            $this->adm->jsAlert("error", "Missing mappings to course codes");
            
        } else if(!$missingMaps) {
            $this->adm->jsAlert("success", "All course codes and categories synched");
            echo "<h5>Course codes and categories all synched. Please verify: <a href='map'>Mapping</a></h5>";
        }
        
        $this->adm->jsAlert("info", "Import Completed");
    }
    
    /**
     *importData Merge temp data from import table into FYS tables
     *
     *@param string[] $row Array of row values
     *@return missing mappings between course codes and categories
     */
    public function importData() {
        // Backup DB
        $this->adm->backupDB('import');
        
        // Push data to CSV and load data into import table
        $this->pushData($this->finalData);
        
        // Import students, instructors, courses
        $crsImported = $this->importCourses();
        
        // Link category and course codes- if previous course codes already defined
        $crsCategoriesLinked = $this->linkCategoryCourseCode();
        
        // Import missing course codes
        $crsCodesLinked = $this->importCourseCodes();
        
        // Truncate table
        $this->clearImportTable();
        
        // Reapply Course's Type (Educators, High School, Maymester)
        $this->reapplyCoursesOf();

        // Display import information
        $this->displayNumRows("Courses Added/Updated: " . $crsImported);
        $this->displayNumRows("Missing Course Codes Added: " . $crsCodesLinked);
        
        return $crsCodesLinked;
    }
    
    /**
     *importCourses Inserts missing courses into courses table or updates existing ones
     *
     *@return int Number of rows imported
     */
    private function importCourses() {
        $query = "INSERT INTO SDB_Courses (ClassNumber, Title, CourseCode, Number, Section, GenEdType, StartDate, EndDate, Instructor, Location, Days, StartTime, EndTime, Term, Active)
                    SELECT SOCNbr, Descr, Subject, Num, Sec, GenEd, StartDate, EndDate, Instructor, Location, CONCAT(M,T,W,R,F,S) as Days, StartTime, EndTime, Term, IF(ClassStat='Active', 1, 0) as Stat
                    FROM SDB_Import import
                    ON DUPLICATE KEY UPDATE Title=import.Descr, Location=import.Location, Days=CONCAT(M,T,W,R,F,S), StartTime=import.StartTime, EndTime=import.EndTime, 
                    StartDate=import.StartDate, EndDate=import.EndDate, Term=import.Term, GenEdType=import.GenEd, Instructor=import.Instructor, Active=IF(ClassStat='Active', 1, 0)";
        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, true, false, true);
        return $response;
    }
    
    private function importCourseCodes() {
        $query = "INSERT INTO SDB_Code_Category_XREF (Code)
                    SELECT DISTINCT Subject 
                    FROM SDB_Import import
                    WHERE NOT EXISTS(SELECT * FROM SDB_Code_Category_XREF cxref WHERE cxref.Code = import.Subject)";
                    
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, true, false, true);
        return $response;
    }
    
    //TODO: test this out- checks for existing category for existing course codes and - not really needed???
    private function linkCategoryCourseCode() {
        $query = "INSERT INTO SDB_Code_Category_XREF (Code, CategoryID)
                    SELECT DISTINCT Subject, cxref.CategoryID
                    FROM SDB_Import import
                    INNER JOIN SDB_Code_Category_XREF cxref ON cxref.Code = import.Subject
                    WHERE cxref.CategoryID IS NOT NULL";
        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, true, false, true);
        return $response;
    }
    
    /**
     *clearImportTable Removes all data from sdb import table
     *
     *@return object Response of query execute
     */
    private function clearImportTable() {
        $query = "TRUNCATE SDB_Import";
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, true, false, true);
        return $response;
    }

    /**
     *reapplyCoursesOf Reapplies all data saved (educator, maymester, high school) prior to the import
     *
     *@return object true
     */
    private function reapplyCoursesOf() {
        $query = "UPDATE `SDB_Courses` c
                  INNER JOIN `SDB_CoursesOf` co ON c.`ClassNumber` = co.`ClassNumber` AND c.`Location` = co.`Location`
                  SET   c.`ForEducators` = co.`ForEducators`,
                        c.`ForHighSchool` = co.`ForHighSchool`,
                        c.`ForMaymester` = co.`ForMaymester`";
        $this->adm->core->executeSQL($query);
        return true;
    }
    
    /**
     *loadDataInfile Imports data to sdb_import for merging
     *
     *@param string $file Import file name
     *@return int Number of rows imported
     */
    private function loadDataInfile($file) {
        $query = "LOAD DATA LOCAL INFILE '" . $file . "' INTO TABLE SDB_Import
                    FIELDS TERMINATED BY '|'
                    LINES TERMINATED BY '\n'";
        
        $response = $this->adm->core->executeSQL($query, false, 'all', 1, true, false, true);
        return $response;
    }
    
    /**
     *writeToCSV Exports import data to baseFileName
     *
     *@param string $file Base file name for output file
     *@param object[] $list Array of import rows and data
     *@return void
     */
    private function writeToCSV($list) {
        // Make sure list is not empty
        $total = count($list);
        if(!$total) {
            return 0;
        }
        
        // Fetch time now and append to file name
        $now = date('y_m_d_h_i_', strtotime("now"));
        $this->absoluteFileName = $_SERVER['DOCUMENT_ROOT'] . $this->baseDir . $now . self::BASE_FILE_NAME;
        
        // Create or rewrite new file
        $fo = fopen($this->absoluteFileName, 'w+');
        
        // Write to file
        foreach($list as $fields) {
            $trimmed = array_map('trim', $fields);
            fputs($fo, implode($trimmed, '|')."\n");
        }
    }
    
    /**
     *displayNumRows Prints out readable message for num rows added/updated
     *
     *@param string $msg Message to display
     *@return void
     */
    private function displayNumRows($msg) {
        echo "<h5 class='text-success'>" . $msg . "</h5>";
    }
}
?>
<?php

date_default_timezone_set('America/New_York');

# Change below variables depending on target environment

if ($_SERVER['HTTP_HOST'] == 'harrisburg2.vmhost.psu.edu') {
    $_settings['devMode'] = false;
    $_settings['dbHost'] = '127.0.0.1';
    $_settings['dbName'] = 'summer_courses';
    $_settings['dbUsername'] = 'summer_courses';
    $_settings['dbPassword'] = '0a56c23d18ce6855c97e177d55a2f768';
} else {
    $_settings['devMode'] = true;
    $_settings['dbHost'] = 'mysql';
    $_settings['dbName'] = 'summer_courses';
    $_settings['dbUsername'] = 'root';
    $_settings['dbPassword'] = 'root';
}

# User Settings values
$_settings['settingKeys'] = array('new_event_email_notification');

# Other settings values
$_settings['developerEmail'] = 'sys106@psu.edu,cjw29@psu.edu,jkv5@psu.edu';
$_settings['developerName'] = 'Simon Song';
$_settings['errorMsg'] = 'Please report the error (stated above) to <a href="mailto:'.$_settings['developerEmail'].'">'.$_settings['developerName'].'</a>.';
$_settings['tablePrefix']='SDB_';

# Used to deal with being hosted on harrisburg3.vmhost- now hosted on harrisburg2
$_settings['assetPrefix'] = 'https://harrisburg2.vmhost.psu.edu';

# Set current url path for linking purposes
preg_match("/(.*)\\/([^\\/]+)/", $_SERVER['SCRIPT_NAME'], $split);
$_settings['current_URL_path'] = $split[1];

?>
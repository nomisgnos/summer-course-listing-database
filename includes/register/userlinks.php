<div class="wrapper">
    <div class="clearfix">
        <div class="column-1">
            <div id="content-header">
            <?php if(isset($accessID)) : ?>
                
                <div style="padding:7.5px;" id="userlinks" class="no-print floatright">
                    <span>Logged in as &nbsp;<strong><?php echo $accessID; ?></strong></span>
                    <ul>
                        <?php 
                            if ($isAdmin) { ?>
                                <li><a href="<?= $_settings['current_URL_path'];?>/admin"><div class="fa fa-dashboard"></div> Admin</a></li>
                        <?php } ?>
                        <li><a href="https://webaccess.psu.edu/cgi-bin/logout"><div class="glyphicon glyphicon-log-out"></div> Logout</a></li>
                    </ul>
                </div><!--.userlinks-->
                
            <?php endif; ?>
            
            <ol style="padding-bottom:7.5px;" class="no-print breadcrumb">
              <li class="breadcrumb-item"><a href="<?= $_settings['current_URL_path'];?>">Summer</a></li>
              <li class="breadcrumb-item active">%BREAD%</li>
            </ol>
            <span class="clearfloat"></span>
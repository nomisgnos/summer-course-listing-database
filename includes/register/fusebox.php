<?php
    require_once('includes/course.php');
    if (empty($userID)) { $userID = 0; }
    $ef = new CourseMethods($obj, $_settings, $userID);

    // check for redirects
    if (isset($_POST) && isset($_POST['Redirect'])) {
        $reflectedMethod = new ReflectionMethod('CourseMethods', $_POST['Redirect']);
        $reflectedMethod->invoke($ef, $_POST);
    }

    echo $buffer;
    ob_flush();
    
    $bread = "Courses";    
    $circ = explode("courses/", $circuit);

    if (!empty($circ[1])) {
        $courseCircuit = $circ[1];
    }
    else {
        $courseCircuit = 'none';
    }
    
    switch ($courseCircuit) {
        case '':    //needed
            $courses = $ef->getCoursesByCategory();
            $byCategory = true;
            break;
        case 'educators':
            $courses = $ef->getEducatorsCourses();
            $bread = "Educators";
            break;
        case 'gen-eds':
            $courses = $ef->getGenEdCourses();
            $bread = "General Education";
            break;
        case 'maymester':
            $courses = $ef->getMaymesterCourses();
            $bread = "Maymester";
            break;
        case 'online':
            $courses = $ef->getOnlineCourses();
            $bread = "Online";
            break;
        case 'highschool':
            $courses = $ef->getHighschoolCourses();
            $bread = "High School";
            break;
        default:
            $courses = $ef->getCoursesByCategory();
            $byCategory = true;
    }

    include 'courselist.php';
    
    $buffer = ob_get_contents();
    ob_clean();
    $buffer = str_replace("%BREAD%", $bread, $buffer);
    echo $buffer;
?>
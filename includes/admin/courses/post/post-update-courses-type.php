<div id="post-id-edit">
<?php
    // validate form ^
    $json = $_POST['json_data'];
    if(empty($json)) {
        return;
    }
    
    $data = json_decode($json);
    $tot = count((array)$data);
    $totUpdated = 0;
    foreach($data as $cid => $types) {
        if(!isset($types->CourseEducator) || !isset($types->CourseHighSchool) || !isset($types->CourseMaymester))
            continue;
        
        $result = $adm->updateCourseTypes($cid, $types);
        $totUpdated += ($result > 0) ? 1 : 0;
    }
    
    $skipped = $tot - $totUpdated;
    if($skipped > 0) {
        $msg = "<strong>Warning:</strong> " . $totUpdated . " row(s) successfully updated. " . $skipped . " row(s) skipped."; 
    } else {
        $msg = "<strong>Success:</strong> " . $totUpdated . " row(s) successfully updated!";
    }
?>


<?=$msg?>
</div>
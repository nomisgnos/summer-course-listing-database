<?php   
    if(!isset($_GET['ID'])) {
        echo "<div class='alert alert-danger'>Error: ID Missing</div>";
        echo "<script>redirectTo('/summer/admin/course/', 1000);</script>";
        return;
    }
    
    $urlDecode = $adm->base64_url_decode($_GET['ID']);
    // Format: CourseID
    $courseExplode = explode(";", $urlDecode);
    $courseid = $courseExplode[0];
    
    $course = $adm->getCourseInfo($courseid);
    $courseInfo = $course[0];
?>

<style>
thead th {
    background: inherit;
    color: inherit;
}
</style>

<div id="alert-area"></div>

<div class="col-md-8">
    <form id="editCourse" class="form-horizontal">
        <fieldset>
            <legend align="left">Edit Course #<?=$courseid?></legend>
            <div class="form-group required">
              <label class="col-md-3 control-label" for="ClassNumber">Class Number</label>  
              <div class="col-md-3">
                <input id="ClassNumber" name="ClassNumber" type="text" value="<?=$courseInfo->ClassNumber?>" class="form-control input-md" required>
                <span class="help-block">Class ID Number</span>
              </div>
            </div>

            <div class="form-group required">
              <label class="col-md-3 control-label" for="Title">Title</label>  
              <div class="col-md-8">
                <input id="Title" name="Title" type="text" value="<?=$courseInfo->Title?>" class="form-control input-md" required>
              </div>
            </div>


            <div class="form-group required">
              <label class="col-md-3 control-label" for="CourseCode">Course Code</label>  
              <div class="col-md-3">
                <input id="CourseCode" name="CourseCode" type="text" value="<?=$courseInfo->CourseCode?>" class="form-control input-md" required>
                <span class="help-block">Ex: KINES, AMST</span>
              </div>
            </div>

            <div class="form-group required">
              <label class="col-md-3 control-label" for="CourseNumber">Course Number</label>  
              <div class="col-md-3">
                <input id="CourseNumber" name="CourseNumber" type="text" value="<?=$courseInfo->Number?>" class="form-control input-md" required>
              </div>
              <label class="col-md-3 control-label" for="Section">Section</label>  
              <div class="col-md-3">
                <input id="Section" name="Section" type="text" value="<?=$courseInfo->Section?>" class="form-control input-md" required>
              </div>
            </div>


            <div class="form-group">
              <label class="col-md-3 control-label" for="StartDate">Start Date</label>  
              <div class="col-md-3">
                <input id="StartDate" name="StartDate" type="text" value="<?=$courseInfo->StartDate?>" class="form-control input-md datepicker">
              </div>
              <label class="col-md-3 control-label" for="EndDate">End Date</label>  
              <div class="col-md-3">
                <input id="EndDate" name="EndDate" type="text" value="<?=$courseInfo->EndDate?>" class="form-control input-md datepicker">
              </div>
            </div>


            <div class="form-group">
              <label class="col-md-3 control-label" for="GenEd">Gen Ed Type</label>  
              <div class="col-md-3">
                <input id="GenEd" name="GenEd" type="text" value="<?=$courseInfo->GenEdType?>" class="form-control input-md">
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="Instructor">Instructor</label>  
              <div class="col-md-4">
                <input id="Instructor" name="Instructor" type="text" value="<?=$courseInfo->Instructor?>" class="form-control input-md">
                <span class="help-block">Format: Last, First M</span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="Location">Location</label>  
              <div class="col-md-3">
                <input id="Location" name="Location" type="text" value="<?=$courseInfo->Location?>" class="form-control input-md">
                <span class="help-block">WEB for online course</span>
              </div>
              <label class="col-md-3 control-label" for="Days">Days</label>  
              <div class="col-md-3">
                <input id="Days" name="Days" type="text" value="<?=$courseInfo->Days?>" class="form-control input-md">
                <span class="help-block">Any of MTWRF without spaces</span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="StartTime">Start Time</label>  
              <div class="col-md-3">
                <input id="StartTime" name="StartTime" type="text" value="<?=$courseInfo->StartTime?>" class="form-control input-md">
              </div>

              <label class="col-md-3 control-label" for="EndTime">End Time</label>  
              <div class="col-md-3">
                <input id="EndTime" name="EndTime" type="text" value="<?=$courseInfo->EndTime?>" class="form-control input-md">
              </div>
            </div>

            <div class="form-group required">
              <label class="col-md-3 control-label" for="Active">Active</label>  
              <div class="col-md-3">
                  <select id="Active" name="Active" class="form-control" required>
                    <option value="0" <?=(!$courseInfo->Active)?"selected":""?>>No</option>
                    <option value="1" <?=($courseInfo->Active)?"selected":""?>>Yes</option>
                  </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="Notes">Notes</label>  
              <div class="col-md-8">
                <textarea id="Notes" name="Notes" class="form-control input-md"><?=$courseInfo->Notes?></textarea>
                <span class="help-block">Viewable to admins only</span>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label" for="submitbtn"></label>
              <div class="col-md-8">
                <button id="submitbtn" name="submitbtn" class="btn btn-success">Submit</button>
                <button id="cancelbtn" name="cancelbtn" class="btn btn-danger">Reset</button>
              </div>
            </div>

        </fieldset>
    </form>
</div>

<script>
    ajaxHandleForm('#editCourse', 'post-edit-course', 'div#post-id-edit', <?=$courseid?>);
</script>
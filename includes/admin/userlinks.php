<?php 
$navVal = (!empty($_COOKIE["menutoggle2"]) && $_COOKIE["menutoggle2"] == "true") ? "nav-sm" : "nav-md"; 
$user = $adm->getName($accessID);
$username = $user[0]->FullName;
?>

<!-- theme from https://github.com/puikinsh/gentelella -->
<body class="<?=$navVal?>">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?=$_settings['current_URL_path'] . '/admin'?>" class="site_title"><i class="fa fa-paw"></i> <span>Summer Courses</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_info">
                <h2>Welcome, <?=$username?></h2>
                <span>Admin</span>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br>

            <!-- sidebar menu -->
            <!-- add pages and sections links to nav here -->
            <?php //MAKE THIS DYNAMIC- see admin.php for more info  ?>
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section active">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li class="default-page"><a href="<?=$_settings['current_URL_path'] . '/admin/course'?>"><i class="fa fa-edit"></i>Courses</span></a></li>
                  <li><a href="<?=$_settings['current_URL_path'] . '/admin/categories'?>"><i class="fa fa-list-alt"></i>Categories</a></li>
                  <li><a href="<?=$_settings['current_URL_path'] . '/admin/map'?>"><i class="fa fa-link"></i>Code <span class="menu-arrow"></span> Category</a></li>
                  <li><a href="<?=$_settings['current_URL_path'] . '/admin/users'?>"><i class="fa fa-user-o"></i>Users</a></li>
                  <li><a href="<?=$_settings['current_URL_path'] . '/admin/import'?>"><i class="fa fa-file-excel-o"></i>Import</a></li>
                  <li><a href="<?=$_settings['current_URL_path'] . '/admin/system'?>"><i class="fa fa-server"></i>System</a></li>
                </ul>
              </div>
              <div class="menu_section">
                <h3>Preview Courses</h3>
                <ul class="nav side-menu">
                  <li><a target="_blank" href="<?=$_settings['current_URL_path'] . '/courses'?>"><i class="fa fa-cubes"></i>By Category</a></li>
                  <li><a target="_blank" href="<?=$_settings['current_URL_path'] . '/courses/educators'?>"><i class="fa fa-mortar-board"></i>Educators</a></li>
                  <li><a target="_blank" href="<?=$_settings['current_URL_path'] . '/courses/gen-eds'?>"><i class="fa fa-info-circle"></i>General Education</a></li>
                  <li><a target="_blank" href="<?=$_settings['current_URL_path'] . '/courses/maymester'?>"><i class="fa fa-sun-o"></i>Maymester</a></li>
                  <li><a target="_blank" href="<?=$_settings['current_URL_path'] . '/courses/online'?>"><i class="fa fa-globe"></i>Online</a></li>
                  <li><a target="_blank" href="<?=$_settings['current_URL_path'] . '/courses/highschool'?>"><i class="fa fa-book"></i>Highschool</a></li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="FullScreen" onclick="toggleFullScreen()">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a class="pull-right" data-toggle="tooltip" data-placement="top" title="" href="https://webaccess.psu.edu/cgi-bin/logout" data-original-title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span> 
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>
            <div class="top_nav">
                  <div class="nav_menu">
                    <nav>
                      <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                      </div>

                      <ul class="nav navbar-nav navbar-right">
                        <li class="">
                          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="images/img.jpg" alt=""><?=$username?>
                            <span class=" fa fa-angle-down"></span>
                          </a>
                          <ul class="dropdown-menu dropdown-usermenu pull-right">
                            <li><a href="https://webaccess.psu.edu/cgi-bin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                          </ul>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>
            </div>
            <div class="right_col" role="main" style="min-height: 1033px;">
                <div class="">
                    <div class="page-title">
                      <div class="title_left">
                        <h3>%TITLE%</h3>
                      </div>
                    </div>
                    <div class="title_right">
                        %TITLE_RIGHT%
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <ol style="padding-bottom:0;" class="no-print breadcrumb">
                          <li class="breadcrumb-item"><a href="<?= $_settings['current_URL_path'];?>/admin">Admin</a></li>
                          <li class="breadcrumb-item active">%BREAD%</li>
                        </ol><!--.breadcrumbs"-->
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">

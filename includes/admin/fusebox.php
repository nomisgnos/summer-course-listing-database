<?php
    require_once('includes/admin.php');
    $adm = new AdminMethods($obj, $_settings, $userID);
    
    // Backup DB if download requested
    if(isset($_POST) && isset($_POST['BackupDB'])) {
        $adm->backupDB('download', true); 
    }
    
    if (isset($_POST) && isset($_POST['Redirect'])) {
        $reflectedMethod = new ReflectionMethod('AdminMethods', $_POST['Redirect']);
        $reflectedMethod->invoke($adm, $_POST);
    }
    
    echo $buffer;
    ob_flush();

    // Show 403 Forbidden error if not admin/instructor/presenter
    if(!$isAdmin) {
        include 'includes/templates/403.php';
        return;
    }
    
    $bread = 'Courses';
    $fuseboxAction = true;
    require_once('userlinks.php');
    
    $title = 'Manage Courses';
    $title_right = '';
    
    switch ($circuit) {
        case '':
        case checkCircuit($circuit, 'course'):
            include 'courses.php';
            break;
        case checkCircuit($circuit, 'users'):
            $bread = 'Users';
            $title = 'Manage Users';
            include 'users.php';
            break;
        case checkCircuit($circuit, 'map'):
            $bread = 'Code Mappings';
            $title = 'Code <i class="fa fa-arrow-right"></i> Category <small>Link course codes to course category.</small>';
            include 'code-category.php';
            break;
        case checkCircuit($circuit, 'import'):
            $bread = 'Import';
            $title = "Import <small>Courses and link course codes to correct categories.</small>";
            include 'import.php';
            break;
        case checkCircuit($circuit, 'system'):
            $bread = 'System';
            $title = 'System';
            include 'system.php';
            break;
        case checkCircuit($circuit, 'categories'):
            $bread = 'Categories';
            $title = 'Categories';
            include 'categories.php';
            break;
        default:
            include 'courses.php';
            $fuseboxAction = false;
            break;
    }
    
    $buffer = ob_get_contents();
    ob_clean();
    $buffer = str_replace("%BREAD%", $bread, $buffer);
    $buffer = str_replace("%TITLE%", $title, $buffer);
    $buffer = str_replace("%TITLE_RIGHT%", $title_right, $buffer);
    echo $buffer;
?>
<script>
if(<?=(!$fuseboxAction) ? "true" : "false"?>) {
    $('.default-page').addClass('current-page');
}
</script>
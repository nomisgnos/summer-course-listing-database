<?php
    // Permissions check
    if (!$isAdmin) {
        return;
    }

    $breadText = '';
    $action = true;
    /* Event Circuits */
    switch($circuit) {
        
        /* Event Actions */
        case checkCircuit($circuit, 'post-user-edit'):
            $breadText = 'Post Edit User';
            include 'users/post/post-user-edit.php';
            break;
        case checkCircuit($circuit, 'post-user-add'):
            $breadText = 'Post Add User';
            include 'users/post/post-user-add.php';
            break;
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Delete User';
            include 'users/delete-user.php';
            break;
        case checkCircuit($circuit, 'edit'):
            $breadText = 'Edit User';
            include 'users/edit-user.php';
            break;
        case checkCircuit($circuit, 'add'):
            $breadText = 'Add User';
            include 'users/add-user.php';
            break;
        
        default:
            $action = false;
    }
    
    if ($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/users">Users</a><li class="breadcrumb-item active">' . $breadText . '</li>';
    } else  {
        $users = $adm->getUsers();
        $title_right = '<a href="' . $_settings['current_URL_path'] . '/admin/users/add" class="btn btn-success btn-add pull-right">New User</a></legend>';
?>

        <div id="alert-area"></div>
        <h2>Admins</h2>
            <table id="tableAdmins" class="table table-hover" style="display:none;">
                <thead>
                    <tr>
                      <th style="display:none;">UID</th>
                      <th width="15%">PSU ID</th>
                      <th width="30%">Name</th>
                      <th width="30%">Email</th>
                      <th width="25%">Actions</th>
                    </tr>
                </thead>
                <tbody>
    <?php
            foreach($users as $user) {
                $constants = $user->UserID . ";" . $user->AccessID . ";" . $user->FName . ";" . $user->LName . ";" . $user->Email;
    ?>
                <tr id="body-message">
                    <td id="body-uid" style="display:none;"><?=$user->UserID?></td>
                    <td id="body-id"><?=$user->AccessID;?></td>
                    <td id="body-name"><?=$user->FName;?> <?=$user->LName;?></td>
                    <td id="body-email"><a href="mailto:<?=$user->Email;?>"><?=$user->Email;?></a></td>
                    <td>
                        <a class="btn btn-primary" style="font-size:12px; margin-right:15px;" href="<?=$_settings['current_URL_path']; ?>/admin/users/edit?ID=<?=$adm->base64_url_encode($constants);?>"><span class="glyphicon glyphicon-edit"></span></a>
                        <a class="btn btn-danger user-delete" href="#" style="font-size:12px;"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
    <?php
            }
    ?>
                </tbody>
            </table>

        
        <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>
        <script>
            $('.table').DataTable({
                columnDefs: [{ "orderable": false, "targets": 4 }, { "searchable": false, "targets": 4 } ],
                order: [[ 2, "asc" ]],
                bDeferRender: true,
                responsive: true,
                autoWidth: false
            });
            $('.table').show();
            $('.table').wrap('<div class="table-responsive"></div>');
            
            ajaxDeleteUser();
        </script>
<?php
    }
?>
<?php
$msg = json_encode($_POST);
if ( !isset($_POST['purgeCourses']) && !isset($_POST['purgeCodeCategories']) && !isset($_POST['purgeAll']) && !isset($_POST['purgeCoursesCategories']) ) {
    $msg = "Error! I don't know what to purge!";
} else {

    $msg = "<strong>Success!</strong> ";
    if(!empty($_POST['purgeCourses']) && $_POST['purgeCourses']) {
        $adm->purgeCourses();
        $msg .= "Purged all courses successfully!";
    } else if(!empty($_POST['purgeCodeCategories']) && $_POST['purgeCodeCategories']) {
        $adm->purgeCodeCategories();
        $msg .= "Purged code to categories successfully!";
    } else if(!empty($_POST['purgeAll']) && $_POST['purgeAll']) {
        $adm->purgeAll();
        $msg .= "Purged all data successfully!";
    } else if(!empty($_POST['purgeCoursesCategories']) && $_POST['purgeCoursesCategories']) {
        $adm->purgeCoursesCategories();
        $msg .= "Purged all course saved categories!";
    }

    //$response = $adm->deleteCourse($id);

}
?>

<div id="post-id-delete">
<?=$msg?>
</div>
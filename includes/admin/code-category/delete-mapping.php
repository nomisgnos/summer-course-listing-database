<?php
if (!$isAdmin) {
    return;
}
    
if (empty($_POST['id'])) {
    $msg = "<strong>Error!</strong> Could not delete mapping!";
} else {
    $adm->deleteCourseCode($_POST['id']);
    // TODO: error handling
    $msg = "<strong>Success!</strong> Mapping: <strong>" . $_POST['id'] . "</strong> deleted!";
}
?>

<div id="post-id-delete">
<?=$msg?>
</div>
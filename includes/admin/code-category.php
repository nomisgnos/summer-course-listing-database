<?php
    // Permissions check
    if (!$isAdmin) {
        return;
    }
        
    if(isset($_POST['AddCourseCategory'])) {
        $categoryName = $_POST['newcategory'];
        $adm->handleAddCourseCategory($categoryName);

    } else if(isset($_POST['AddCourseCode'])) {
        $courseCode = $_POST['newcoursecode'];
        $adm->handleAddCourseCode($courseCode);
    }
    
    $categoryOptions = $adm->getCourseCategories(true);
    $codes = $adm->getCourseCodes();
    
    switch($circuit) {
        /* Mapping Actions */
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Delete mapping';
            include 'code-category/delete-mapping.php';
            break;
        default:
            break;
    }
    
    $originalOptions = $adm->buildCodeToCategoryOptions($codes);
    if(isset($_POST['SubmitForm'])){
        $updateValues = array_diff_assoc($_POST, $originalOptions);
        unset($updateValues['SubmitForm']);

        if(empty($updateValues)) {
            $adm->jsAlert("error", "Changes not found. Make some updates first!");
        } else {
            echo "<meta http-equiv='refresh' content='0' />";
            $adm->handleUpdateCodeMappings($updateValues);
        }
        
    }
?>

<div class="row">
    <form class="form-inline col-md-4" id="AddNewCourseCode" action="" method="post">
        <div class="form-group input-group">
            <input type="text" class="form-control" id="newcoursecode" name="newcoursecode" placeholder="Course code" required></input>
            <span class="input-group-btn">
                <button class="btn btn-success" type="submit" name="AddCourseCode" value="Add" >Add</button>  
            </span>
        </div>
    </form>
    
    <form class="form-inline col-md-4 col-md-offset-4" id="AddNewCategory" action="" method="post">
        <div class="form-group input-group">
            <input type="text" class="form-control" id="newcategory" name="newcategory" placeholder="Course category" required></input>
            <span class="input-group-btn">
                <button class="btn btn-success" type="submit" name="AddCourseCategory" value="Add" >Add</button>  
            </span>
        </div>
    </form>
</div>

<hr class="style1">
<div style="margin-top:20px;" class="row">
    <form id="submitMapppingForm" action="" method="post">
        <table id="dataTable" class="table table-hover table-data">
            <thead>
                <tr>
                  <th style="display:none;"></th>
                  <th>Course Code</th>
                  <th>Category</th>
                  <th></th>
                </tr>
            </thead>
            
            <tbody>
        <?php
                foreach($codes as $code) {
        ?>
                    <tr class="CourseCategory<?=$code->ID?>"" id="body-message">
                        <td style="display:none;" id="body-id"><?=$code->ID?></td>
                        <td id="body-course-code"><?=$code->Code?></td>
                        <td id="body-course-categ">
                            <select id="CourseCategory<?=$code->ID?>" data-container="body" name="CourseCategory<?=$code->ID?>" title="Select Category" class="selectcategory form-control">
                                <option value="0" disabled selected>Select Category</option>
                                <?=$adm->setSelectedCategory($categoryOptions, $code->CategoryID)?>
                            </select>
                        </td>
                        <td id="">
                            <a href="#" id="deleteCode" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
        <?php
                }
        ?>
            </tbody>
        </table>
    
        <div class="pull-left"><button class="btn btn-success" type="submit" name="SubmitForm" value="Apply" >Apply</button>&nbsp;&nbsp;&nbsp;<button name="reset" class="btn btn-danger" type="reset" value="Reset" >Reset</button></div>
    </form>
</div>

<script>
function highlightNotSelected() {
    $(".selectcategory").each(function() {
        if(!$(this).val()) {
            $('.' + $(this).attr("id")).addClass("bg-danger");
        }
    });
}

highlightNotSelected();

$(".selectcategory").change(function () {
    if($(this).val()) {
        $('.' + $(this).attr("id")).removeClass("bg-danger");
    }
});

$('#submitMapppingForm').on('reset', function() {
  setTimeout(function() {
    highlightNotSelected();
  });
});

ajaxDeleteMapping();
</script>

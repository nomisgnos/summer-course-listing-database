<?php
    // Permissions check
    if (!$isAdmin) {
        return;
    }
    $breadText = '';
    $action = true;
    
    
    /* Event Circuits */
    switch($circuit) {
        case checkCircuit($circuit, 'delete'):
            $breadText = 'Delete course';
            include 'courses/delete-course.php';
            break;
        case checkCircuit($circuit, 'post-update-type'):
            $breadText = 'Post Update Course Type';
            include 'courses/post/post-update-courses-type.php';
            break;
        case checkCircuit($circuit, 'post-edit-course'):
            $breadText = 'Post Edit Event';
            include 'courses/post/post-edit-course.php';
            break;
        case checkCircuit($circuit, 'post-add-course'):
            $breadText = 'Post Add Event';
            include 'courses/post/post-add-course.php';
            break;
        /* Event Actions */
        case checkCircuit($circuit, 'edit'):
            $breadText = 'Edit course';
            include 'courses/edit-course.php';
            break;
        case checkCircuit($circuit, 'add'):
            $breadText = 'Add course';
            include 'courses/add-course.php';
            break;
        default:
            $action = false;
    }
    
    if ($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/courses">Courses</a><li class="breadcrumb-item active">' . $breadText . '</li>';
    } else  {
        $courses = $adm->getCourses();
        
        $title_right = '<a href="' . $_settings['current_URL_path'] . '/admin/course/add" class="btn btn-success btn-add pull-right" >New Course</a>';
?>

        <div id="alert-area"></div>
        <table id="dataCourses" class="table hover" width="100%" style="display:none;">
            <thead>
                <tr>
                    <th style="width:80px;">Class #</th>
                    <th>Course</th>
                    <th>Title</th>
                    <th>Days</th>
                    <th>Gen Ed</th>
                    <th>Educator</th>
                    <th>High School</th>
                    <th>Maymester</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
        <?php
            $defaultRows = [];
            foreach($courses as $course) {
                // Format: CourseID
                $constants = $course->CourseID;
                $encoded = $adm->base64_url_encode($constants);
                
                $defaultRows[$course->CourseID]["CourseEducator"] = (int)$course->ForEducators;
                $defaultRows[$course->CourseID]["CourseHighSchool"] = (int)$course->ForHighSchool;
                $defaultRows[$course->CourseID]["CourseMaymester"] = (int)$course->ForMaymester;
                
        ?>      <tr id="<?=$course->CourseID?>" class="course-row">
                    
                    <td><span id="body-id" style="display:none;"><?=$course->CourseID?></span><span style="margin-top:5px; font-size:13px; cursor:pointer;" data-delay='{"show":0, "hide":100}' data-placement="auto left" data-toggle="tooltip" title="Copy to clipboard" data-original-title="Copy to clipboard" class="label label-default clipme" data-clipboard-text="<?=$course->ClassNumber?>"><?=$course->ClassNumber?></span></td>
                    <td id="body-title"><?=$course->CourseCode?> <?=$course->Number?>.<?=$course->Section?></td>
                    <td id="body-message"><?=$course->Title?></td>
                    <td><?=empty($course->Days) ? $course->Location : $course->Days?></td>
                    <td><?=$course->GenEdType?></td>
                    <td>
                        <span style="display:none;"><?=$course->ForEducators?></span>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-crsid="<?=$course->CourseID?>" name="CourseEducator" value="1" <?=$course->ForEducators ? "checked" : ""?>></input>
                                <span class="cr" id="CourseEducator<?=$course->CourseID?>"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                            </label>
                        </div>
                    </td>
                    <td>
                        <span style="display:none;"><?=$course->ForHighSchool?></span>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-crsid="<?=$course->CourseID?>" name="CourseHighSchool" value="1" <?=$course->ForHighSchool ? "checked" : ""?>></input>
                                <span class="cr" id="CourseHighSchool<?=$course->CourseID?>"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                            </label>
                        </div>
                    </td>
                    <td>
                        <span style="display:none;"><?=$course->ForMaymester?></span>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-crsid="<?=$course->CourseID?>" name="CourseMaymester" value="1" data-order="" <?=$course->ForMaymester ? "checked" : ""?>></input>
                                <span class="cr" id="CourseMaymester<?=$course->CourseID?>"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                            </label>
                        </div>
                    </td>
                    <td>
                        <a class="btn btn-primary btn-edit" style="" href="<?=$_settings['current_URL_path']; ?>/admin/course/edit?ID=<?=$encoded?>"><span class="glyphicon glyphicon-edit"></span></a>
                        <a class="btn btn-danger course-delete btn-delete" href="#"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                    <td><?=!$course->Active?'<span class="label label-danger">Inactive</span>':''?></td>
                </tr>
        <?php
            }
        ?>
           
            </tbody>
        </table>


<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_settings['current_URL_path'];?>/resources/datatables/dataTables.bootstrap.min.js"></script>

<?php //TODO: move js below to another file and minify ?>
<script>
    // Clipboard for schedule number
    $('.clipme').click(function(){
        var e = this;
        copyToClipboard(e);
        $(e).attr('data-original-title', 'Copied!').tooltip('show');
        setTimeout(function() { $(e).attr('data-original-title', 'Copy to clipboard!').tooltip('hide');}, 500);
    });
    
    // Create trigger for delete course event
    ajaxDeleteCourse();
    
    var defaultRows = <?=json_encode($defaultRows)?>;
    var selectedRows = {};
    
    var addBorder = "2px solid #1aaa55";
    var defBorder = "1px solid #a9a9a9";
    var delBorder = "2px solid #db3b21";
    
    function syncDefaultRows(rows) {
        // sync default rows with selected rows
        for(var crsid in rows) {
            defaultRows[crsid] = JSON.parse(JSON.stringify(selectedRows[crsid]));
        }
        
        // reset selected rows object
        delete selectedRows;
        selectedRows = {};
        
        //reset border and rows text
        changeBorder(0, 0, defBorder, true);
        $('.label-rows').text("0 rows");
    }
    
    function changeBorder(row, col, border, reset=false) {
        if(reset) {
            $('.cr').css('border', border);
            return;
        }
        
         $('#' + col + row).css('border', border);
    }
    
    /* Courses DataTable */
    var t = $('#dataCourses').DataTable({
        bDeferRender: true,
        columnDefs: [{ "orderable": false, "targets": [8] }, { "searchable": false, "targets": [8] } ],
        order: [[ 2, "asc" ]],
        lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        orderClasses: false,
        responsive: true,
        autoWidth: true,
        search: {smart: false},
        drawCallback: function( settings ) {
            rowsChanged = Object.keys(selectedRows).length;
            
            btn = '<button type="submit" style="float:left; display:inline-block;" class="btn btn-success btn-submit-crs-form" href="#">Apply</button> <span class="label label-primary label-rows" style="margin-left:2px;float:left;display:inline-block;">' + rowsChanged + ' rows</span>';
            $("div.drawBtn").html(btn);
            
            $('input[type="checkbox"]').click(function(){
                rowID = $(this).data('crsid');
                
                if(selectedRows[rowID] === undefined) {
                    selectedRows[rowID] = JSON.parse(JSON.stringify(defaultRows[rowID]));
                }
                
                colName = $(this).attr('name');
                selectedRows[rowID][colName] = $(this).is(':checked') ? 1 : 0;
                
                if(selectedRows[rowID][colName] && !defaultRows[rowID][colName]) {
                    changeBorder(rowID, colName, addBorder);
                    $('#' + colName + rowID).css('border', addBorder);
                } else if(!selectedRows[rowID][colName] && defaultRows[rowID][colName]){
                    changeBorder(rowID, colName, delBorder);
                } else {
                    changeBorder(rowID, colName, defBorder);
                }
                
                if(JSON.stringify(selectedRows[rowID]) == JSON.stringify(defaultRows[rowID])) {
                    delete selectedRows[rowID];
                }
                
                $('.label-rows').text(Object.keys(selectedRows).length + " rows");
            });
        },
        dom: '<"row"f><"clearfix"><"row view-filter"<"col-sm-8"<"pull-left"l><"pull-right"><"clearfix">><"col-sm-4"<"drawBtn">>>t<"row view-pager"<"col-sm-8"<"pull-left"i>><"col-sm-4 drawBtn">><"row pull-left"p>'
    });
    
    function btnSubmitCrsForm() {
        $('.btn-submit-crs-form').click(function() {
            if(!Object.keys(selectedRows).length) {
                return newAlert('warning', 'No changes found', 'Warning');
            }

            $.ajax({
                type: 'POST',
                url: 'course/post-update-type',
                data: {json_data: JSON.stringify(selectedRows)},
                success: function(data) {
                    var status = $(data).find("div#post-id-edit").html();

                    if (status === undefined) {
                        newAlert('error', "Encountered an error!", "Error");
                    } else if (status.toLowerCase().indexOf("success") >= 0) {
                        newAlert('success', status, '');
                        // sync default rows after update
                        syncDefaultRows(selectedRows);
                    } else {
                        newAlert('warning', status, 'Something went wrong');
                    }
                }
            }).fail(function() {
                newAlert('error', "Encountered an error!", 'Error');
            });
        });
    }
    
    btnSubmitCrsForm();
    t.on('draw.dt', function () {
        btnSubmitCrsForm();
    });
    
    $('#dataCourses').show();
    $('.table').wrap('<div class="table-responsive"></div>');
    
    $('[data-toggle="tooltip"]').tooltip();
</script>
    
<?php } ?>
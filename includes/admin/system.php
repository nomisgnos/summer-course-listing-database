<?php
    $action = false;
    switch($circuit) {
        case checkCircuit($circuit, 'delete'):
            $action = true;
            $breadText = 'Purge data';
            include 'system/purge-data.php';
            break;
        default:
            break;
    }

    if($action) {
        return;
    }
?>
    <h4>Backup</h4>
    <div class="row">
        <form method="post">
            <button class="btn btn-success" type="submit" name="BackupDB"><i class="fa fa-database"></i> Database</button>
        </form>
    </div>
</div>

<div class="x_panel">
    <h4>Purge Data</h4>
    <div class="row">
        <button class="btn btn-danger" id="deleteCourses"><i class="fa fa-remove"></i> Courses</button>
        <button class="btn btn-danger" id="deleteCoursesCategories"><i class="fa fa-remove"></i> Courses Saved Types*</button>
        <button class="btn btn-danger" id="deleteCodeToCategory"><i class="fa fa-unlink"></i> Code <i class="fa fa-arrow-right"></i> Category</button>
        <button class="btn btn-danger" id="deleteAll"><i class="fa fa-trash"></i> ALL DATA</button>
    </div>
    <div style="margin:1em 0; border:1px solid #cacaca;padding:1em;">
      <h5>Key:</h5>
      <p>
        * = "Educators", "High School", "Maymester"
      </p>
    </div>
    <script>
        ajaxPurgeData();
    </script>
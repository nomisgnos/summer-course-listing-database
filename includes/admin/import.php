<?php
    // Permissions check
    if (!$isAdmin) {
        return;
    }

    $action = false;
    
    switch ($circuit) {
        case checkCircuit($circuit, 'timeline'):
            include 'import/timeline.php';
            $breadText = "Timeline";
            $action = true;
            break;
    }
    
    if($action) {
        $bread = '<a style="text-decoration: none;" href="' . $_settings['current_URL_path'] . '/admin/import">Import</a><li class="breadcrumb-item active">' . $breadText . '</li>';
        return;
    }
    
    require_once('includes/import.php');
    require_once('import/excel_reader.php');
    
    $import = new ImportMethods($adm);
    
    
    if(!empty($_POST['SubmitHeaderMapForm'])) {
        unset($_POST['SubmitHeaderMapForm']);
        
        $import->reformatData($_POST);
        $missingMaps = $import->importData();
        $import->verifyImport($missingMaps);
    }
    
    if(isset($_FILES['xlsfile'])) {
        $errors= array();
        $file_name = $_FILES['xlsfile']['name'];
        $file_size = $_FILES['xlsfile']['size'];
        $file_tmp = $_FILES['xlsfile']['tmp_name'];
        $file_type = $_FILES['xlsfile']['type'];
        $explodeFileName = explode('.',strtolower($file_name));
        $file_ext = end($explodeFileName);

        $allowedExtensions = array("xls","csv");

        if(in_array($file_ext, $allowedExtensions)=== false){
            $errors[] = "Extension not allowed- upload a XLS or CSV file";
        }
        
        if($file_size > 2097152) {
            $errors[] = 'File size must be less than 2 MB';
        }
        
        $newFileName = $import->fileLocation . $file_name;
        if(!empty($errors)) {
            echo "<h3 style='color:red'>";
            foreach($errors as $err)
                echo $err . '<br>';
            echo "</h3>";
        }
        
        move_uploaded_file($file_tmp , $newFileName);
        
        $dataFound = $import->excelParseFile($file_name, $_POST['headerNumber']);

        if(!$dataFound)
            return;
        
        $import->verifyImport($missingMaps);
    }
    
    $title = "Import <small>Courses and link course codes to correct categories.</small>";
    
    // TODO- finish import timeline
    //$title_right = '<a href="' . $_settings['current_URL_path'] . '/admin/import/timeline" class="btn btn-primary pull-right">Import Timeline</a>';
?>

<div class="clearfix">
    <div>
        <p class="alert alert-danger" style="position:relative;border-radius: 4px;"><strong>!!WARNING!!</strong> For quality assurance and for data integrity purposes, this import <u>WILL NOT</u> purge the "Categories" nor the "Courses" table. This tool will update existing data, add course codes/courses that don't exist, and connect the information to each other based off the XLS file. <br><br>Any manually entered data (course code to category relationship) <u>WILL NOT</u> be touched.  If you have any questions, please contact Simon Song or Chris Weaver.</p>
        <form action="" enctype="multipart/form-data" method="post">
            <div class="form-group">
                <label for="xlsfile">File</label>
                <input class="form-control" type="file" name="xlsfile" id="xlsfile">
            </div>
            <div class="form-group">
                <label for="headerNumber">Header Row Number</label>
                <input class="form-control" type="text" name="headerNumber" value="1">
            </div>
            
            <button type="submit" class="btn btn-success">Upload XLS (97-2003 ONLY) File</button>
        </form>

    </div><br>
    <p>If you have a worksheet that's XLSX or a newer version of Excel, please save it as a 97-2003 Worksheet.  If you are NOT certain what version it's using, it's best to save it as a 97-2003 worksheet for the best results.</p>
</div>

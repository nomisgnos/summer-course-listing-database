<?php
// Permissions check- todo: update user access ids
if (!$isAdmin) {
    return;
}
    
if (!isset($_POST['uid']) || !isset($_POST['id']) || empty($_POST['uid']) || empty($_POST['id'])) {
    $msg = "<strong>Error!</strong> Could not delete user!";
} else {
    $adm->deleteUser($_POST['uid']);
    // TODO: error handling
    $msg = "<strong>Success!</strong> Access ID: <strong>" . $_POST['id'] . "</strong> deleted!";
}
?>

<div id="post-id-delete">
<?=$msg?>
</div>
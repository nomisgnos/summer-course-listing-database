<style>
.alert {
    position:fixed; 
    text-align:center;
    top: 0px; 
    left: 0px; 
    width: 100%;
    z-index:9999; 
    border-radius:0px
}
</style>

<div id="alert-area"></div>
<div class="col-md-8">
<form id="addUser" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Add User</legend>

<div class="form-group required">
  <label class="col-md-3 control-label" for="accessid">Access ID:</label>  
  <div class="col-md-8">
    <input id="accessid" name="accessid" type="text" placeholder="abc123" class="form-control input-md" required="" autofocus>
  </div>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="firstname">First Name</label>  
  <div class="col-md-9">
    <input id="firstname" name="firstname" type="text" class="form-control input-md" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="lastname">Last Name</label>  
  <div class="col-md-9">
  <input id="lastname" name="lastname" type="text" class="form-control input-md" required="">
  </div>
</div>

<!-- Text input-->
<div class="form-group required">
  <label class="col-md-3 control-label" for="email">Email</label>  
  <div class="col-md-9">
  <input id="email" name="email" type="text" placeholder="abc123@psu.edu" class="form-control input-md" required="">
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary btn-raised">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger btn-raised" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
    $("#roles").selectpicker("render");
    ajaxHandleForm('#addUser', 'post-user-add', 'div#post-id-add');
</script>
<?php   
    $urlDecode = $adm->base64_url_decode($_GET['ID']);
    // Format: UserID, AccessID, FName, LName, Email
    $user = explode(";", $urlDecode);
    $userid = $user[0];
?>

<div id="alert-area"></div>
<div class="col-md-8">
<form id="editUser" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend align="left">Edit User: <?=$user[2]?> <?=$user[3]?></legend>


<div class="form-group">
  <label class="col-md-3 control-label" for="accessid">Access ID:</label>  
  <label class="col-md-2 control-label" for="accessid"><?=$user[1]?></label>  
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="firstname">First Name</label>  
  <div class="col-md-8">
    <input id="firstname" name="firstname" type="text" value="<?=$user[2]?>" class="form-control input-md" autofocus>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="lastname">Last Name</label>  
  <div class="col-md-8">
  <input id="lastname" name="lastname" type="text" value="<?=$user[3]?>" class="form-control input-md">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="email">Email</label>  
  <div class="col-md-8">
  <input id="email" name="email" type="text" value="<?=$user[4]?>" class="form-control input-md" required="">
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-3 control-label" for="submitbtn"></label>
  <div class="col-md-8">
    <button id="submitbtn" name="submitbtn" class="btn btn-primary">Submit</button>
    <button id="cancelbtn" name="cancelbtn" class="btn btn-danger" type="reset">Reset</button>
  </div>
</div>

</fieldset>
</form>
</div>

<script>
    $("#roles").selectpicker("render");
    ajaxHandleForm('#editUser', 'post-user-edit', 'div#post-id-edit', <?=$userid?>);
</script>
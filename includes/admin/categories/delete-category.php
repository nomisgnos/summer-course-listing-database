<?php
if (!$isAdmin) {
    return;
}
    
if (empty($_POST['id'])) {
    $msg = "<strong>Error!</strong> Could not delete category!";
} else {
    $adm->deleteCategory($_POST['id']);
    // TODO: error handling
    $msg = "<strong>Success!</strong> Category: <strong>" . $_POST['id'] . "</strong> deleted!";
}
?>

<div id="post-id-delete">
<?=$msg?>
</div>
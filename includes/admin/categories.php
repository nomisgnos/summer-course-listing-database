<?php
$categories = $adm->getCourseCategories();
$originalOptions = $adm->buildCategoryNames($categories);
    
switch($circuit) {
    /* Actions */
    case checkCircuit($circuit, 'delete'):
        $breadText = 'Delete category';
        include 'categories/delete-category.php';
        break;
    default:
        break;
}
    
if(isset($_POST['AddCourseCategory'])) {
    $categoryName = $_POST['newcategory'];
    $adm->handleAddCourseCategory($categoryName);
    echo "<meta http-equiv='refresh' content='0' />";
} else if(isset($_POST['SubmitForm'])){
    $updateValues = array_diff_assoc($_POST, $originalOptions);
    unset($updateValues['SubmitForm']);
    if(empty($updateValues)) {
        $adm->jsAlert("error", "Changes not found. Make some updates first!");
    } else {
        echo "<meta http-equiv='refresh' content='0' />";
        // todo: alert always gets displayed before refresh- should be displayed after meta refresh
        // solution: convert to ajax
        $adm->handleUpdateCategories($updateValues);    
    }
}
?>
<div class="row">
    <form class="form-inline col-md-12" id="AddNewCategory" action="" method="post">
        <div class="form-group input-group">
            <input type="text" class="form-control" id="newcategory" name="newcategory" placeholder="Course category" required></input>
            <span class="input-group-btn">
                <button class="btn btn-success" type="submit" name="AddCourseCategory" value="Add" >Add</button>  
            </span>
        </div>
    </form>
</div>

<div style="margin-top:20px;" class="row">
    <form id="submitMapppingForm" action="" method="post">
        <table id="dataTable" class="table table-hover table-data">
            <thead>
                <tr>
                  <th style="width:50px;">ID</th>
                  <th>Category</th>
                  <th></th>
                </tr>
            </thead>
            
            <tbody>
        <?php
                foreach($categories as $category) {
        ?>
                    <tr class="CourseCategory<?=$category->CategoryID?>" id="body-message">
                        <td id="body-id"><strong><?=$category->CategoryID?></strong></td>
                        <td id="body-course-categ"><input id="CourseCategory<?=$category->CategoryID?>" name="CourseCategory<?=$category->CategoryID?>" class="form-control" type="text" value="<?=$category->Name?>"></input></td>
                        <td><a href="#" id="deleteCtg" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
                    </tr>
        <?php
                }
        ?>
            </tbody>
        </table>
    
        <div class="pull-left"><button class="btn btn-success" type="submit" name="SubmitForm" value="Apply" >Apply</button>&nbsp;&nbsp;&nbsp;<button name="reset" class="btn btn-danger" type="reset" value="Reset" >Reset</button></div>
    </form>
</div>

<script>
ajaxDeleteCategory();
</script>